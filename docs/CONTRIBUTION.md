
## Contribution

### If you're part of the projet
-   clone the project on your local repository
-   create your branch like author/feature
-   work in your local branch
-   when your work is ready, create a merge request to 'dev' branch

### If you're not part of the project but the project is public
-   fork from this main repository
-   try to handle on of the open issues
-   write your code and don't forget the tests
-   open a merge request

### Recommended practices
-   Run a code quality to avoid style and bad practice issues (this project uses Codacy)
-   Use Symfony documentation to stick to intended use of the framework
-   Write run and pass tests
-   Try clean code as much as possible (meaningful variable and method names, single responsibility, short methods...)

## Performance
You can chase performance issues with blackfire profiler and/or the embedded symfony profiler, performance issues and possible fix will be tagged 'optimization' in the issues of the project
