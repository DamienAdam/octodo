<?php

namespace App\Tests\Func;

use App\DataFixtures\AppFixtures;
use App\Entity\User;
use App\Repository\UserRepository;
use Liip\TestFixturesBundle\Test\FixturesTrait;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\HttpFoundation\Request;

class UserTest extends KernelTestCase
{
    use FixturesTrait;

    public function testCountUsers()
    {
        self::bootKernel();
        $this->loadFixtures([AppFixtures::class]);

        $users = self::$container->get(UserRepository::class)->count([]);

        $this->assertEquals(12, $users);
    }
}