<?php
/**
 * Short description here.
 *
 * PHP version 7.4
 *
 * @category Repository
 * @package None
 * @author Damien <private@dontemailme.com>
 * @copyright Stealthis
 * @license None
 * @link http://google.com
 */

namespace App\Repository;

use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * Class UserRepository
 * @package App\Repository
 */
class UserRepository extends ServiceEntityRepository
{
    /**
     * UserRepository constructor.
     * @param ManagerRegistry $registry
     */
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, User::class);
    }
}