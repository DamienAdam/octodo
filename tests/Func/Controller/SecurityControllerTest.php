<?php

namespace App\Tests\Func\Controller;

use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class SecurityControllerTest extends WebTestCase
{

    public function testLoginAction()
    {
        $client = $this->createClient();
        $crawler = $client->request(Request::METHOD_GET, '/login');

        $this->assertResponseStatusCodeSame(Response::HTTP_OK);
    }

    public function testLoginFailAction()
    {
        $client = $this->createClient();
        $crawler = $client->request(Request::METHOD_GET, '/login');

        $form = $crawler->selectButton('Se connecter')->form([
            '_username' => 'user',
            '_password' => '$2y$10$sTVcaXcZ2kH3vGVJxh4WWev9ThebVwZyPK5IAbWrtPaqSA/2qfKXi'
        ]);

        $client->submit($form);

        $this->assertResponseRedirects($crawler->getBaseHref(), 302);
    }

    public function testLoginSuccessAction()
    {
        $client = $this->createClient();
        $crawler = $client->request(Request::METHOD_GET, '/login');

        $form = $crawler->selectButton('Se connecter')->form([
            '_username' => 'user',
            '_password' => '$2y$10$oFd6PF6TwmV0dKlPOb8zOOyZ.Tegfe4v7c2bKooVeTlpZjrn7DMLm'
        ]);

        $client->submit($form);
        $this->assertResponseRedirects($crawler->getBaseHref());
    }
}