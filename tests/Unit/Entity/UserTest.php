<?php

namespace App\Tests\Unit\Entity;

use App\Entity\Task;
use App\Entity\User;
use PHPUnit\Framework\TestCase;

class UserTest extends TestCase
{
    private $user;

    protected function setUp(): void
    {
        parent::setUp();

        $this->user = new User();
    }

    public function testGetEmail(): void
    {
        $setterReturn = $this->user->setEmail($value = 'test@test.fr');

        $this->assertInstanceOf(User::class, $setterReturn);
        $this->assertEquals($value, $this->user->getEmail());
    }

    public function testGetUsername(): void
    {
        $setterReturn = $this->user->setUsername($value = 'username');

        $this->assertInstanceOf(User::class, $setterReturn);
        $this->assertEquals($value, $this->user->getUsername());
    }

    public function testGetRoles(): void
    {
        $setterReturn = $this->user->setRoles([User::ROLE_ADMIN]);

        $this->assertInstanceOf(User::class, $setterReturn);
        $this->assertContains(User::ROLE_USER, $this->user->getRoles());
        $this->assertContains(User::ROLE_ADMIN, $this->user->getRoles());
    }

    public function testGetRolesWithAdd(): void
    {
        $setterReturn = $this->user->addRoles(User::ROLE_ADMIN);

        $this->assertInstanceOf(User::class, $setterReturn);
        $this->assertContains(User::ROLE_USER, $this->user->getRoles());
        $this->assertContains(User::ROLE_ADMIN, $this->user->getRoles());
    }

    public function testGetSalt(): void
    {
        $this->assertInstanceOf(User::class, $this->user);
        $this->assertEquals(null, $this->user->getSalt());
    }

    public function testGetPassword(): void
    {
        $setterReturn = $this->user->setPassword($value = 'password');

        $this->assertInstanceOf(User::class, $setterReturn);
        $this->assertEquals($value, $this->user->getPassword());
    }

    public function testGetTask(): void
    {
        $task = new Task();
        $setterReturn = $this->user->addTask($task);

        $this->assertInstanceOf(User::class, $setterReturn);
        $this->assertCount(1, $this->user->getTasks());
        $this->assertTrue($this->user->getTasks()->contains($task));

        $this->user->removeTask($task);
        $this->assertInstanceOf(User::class, $setterReturn);
        $this->assertCount(0, $this->user->getTasks());
        $this->assertFalse($this->user->getTasks()->contains($task));

    }
}