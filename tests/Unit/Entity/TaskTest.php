<?php


namespace App\Tests\Unit\Entity;


use App\Entity\Task;
use PHPUnit\Framework\TestCase;

class TaskTest extends TestCase
{
    private $task;

    protected function setUp(): void
    {
        parent::setUp();

        $this->task = new Task();
    }

    public function testGetCreatedAt(): void
    {
        $setterReturn = $this->task->setCreatedAt($value = new \Datetime('2020-04-15 10:55:24'));

        $this->assertInstanceOf(Task::class, $setterReturn);
        $this->assertEquals($value, $this->task->getCreatedAt());
    }

    public function testGetTitle()
    {
        $setterReturn = $this->task->setTitle($value = 'test-title');

        $this->assertInstanceOf(Task::class, $setterReturn);
        $this->assertEquals($value, $this->task->getTitle());
    }

    public function testGetContent()
    {
        $setterReturn = $this->task->setContent($value = 'lorem ipsum dolor sit amet');

        $this->assertInstanceOf(Task::class, $setterReturn);
        $this->assertEquals($value, $this->task->getContent());
    }

    public function testIsDone()
    {
        $setterReturn = $this->task->toggle(false);

        $this->assertInstanceOf(Task::class, $setterReturn);
        $this->assertFalse($this->task->isDone());

        $this->task->toggle(false);

        $this->assertFalse($this->task->isDone());
    }
}