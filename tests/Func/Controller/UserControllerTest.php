<?php

namespace App\Tests\Func\Controller;

use App\Entity\User;
use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\BrowserKit\Cookie;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;

class UserControllerTest extends WebTestCase
{
    public function testListActionDenied()
    {
        $client = $this->createClient();

        $users = $client->getContainer()->get(UserRepository::class);
        $user = $users->findOneBy([
            'username' => 'user'
        ]);

        $session = $client->getContainer()->get('session');
        $token = new UsernamePasswordToken($user, null, 'main', $user->getRoles());
        $session->set('_security_main', serialize($token));
        $session->save();
        $cookie = new Cookie($session->getName(), $session->getId());
        $client->getCookieJar()->set($cookie);

        $crawler = $client->request(Request::METHOD_GET, '/admin/users');

        $this->assertResponseStatusCodeSame(Response::HTTP_FORBIDDEN);
    }

    public function testListAction()
    {
        $client = $this->createClient();

        $users = $client->getContainer()->get(UserRepository::class);
        $user = $users->findOneBy([
            'username' => 'admin'
        ]);

        $session = $client->getContainer()->get('session');
        $token = new UsernamePasswordToken($user, null, 'main', $user->getRoles());
        $session->set('_security_main', serialize($token));
        $session->save();
        $cookie = new Cookie($session->getName(), $session->getId());
        $client->getCookieJar()->set($cookie);

        $crawler = $client->request(Request::METHOD_GET, '/admin/users');

        $this->assertResponseStatusCodeSame(Response::HTTP_OK);
    }

    public function testCreateDenied()
    {
        $client = $this->createClient();
        $user = $this->logUser($client, 'user');
        $crawler = $client->request(Request::METHOD_GET, '/admin/users/create');

        $this->assertResponseStatusCodeSame(Response::HTTP_FORBIDDEN);

    }

    public function testCreateWithRole()
    {
        $client = $this->createClient();
        $this->logUser($client, 'admin');
        $crawler = $client->request(Request::METHOD_GET, '/admin/users/create');

        $form = $crawler->selectButton('Ajouter')->form([
            'user[username]' => 'testcreate',
            'user[email]' => 'testcreate@test.com',
            'user[password][first]' => 'testcreatepassword',
            'user[password][second]' => 'testcreatepassword'
        ]);

        $form['user[roles]'][1]->tick();

        $client->submit($form);
        $client->followRedirect();

        /** @var UserRepository $repo */
        $repo = $client->getContainer()->get(UserRepository::class);

        /** @var User $task */
        $newUser = $repo->findOneBy([
            'username' => 'testcreate',
        ]);

        $this->assertContains(User::ROLE_ADMIN, $newUser->getRoles());
    }

    public function testEditWithRole()
    {
        $client = $this->createClient();
        $user = $this->logUser($client, 'admin');
        $crawler = $client->request(Request::METHOD_GET, '/admin/users/create');

        /** @var UserRepository $repo */
        $repo = $client->getContainer()->get(UserRepository::class);

        /** @var User $task */
        $newUser = $repo->findOneBy([
            'username' => 'testcreate',
        ]);

        $crawler = $client->request(Request::METHOD_GET, '/admin/users/'.$newUser->getId().'/edit');
        $form = $crawler->selectButton('Modifier')->form();

        $form['user[roles]'][0]->tick();
        $form['user[roles]'][1]->untick();

        $client->submit($form);
        $client->followRedirect();

        /** @var User $task */
        $newUser = $repo->findOneBy([
            'username' => 'testcreate',
        ]);

        $roles = $newUser->getRoles();
        $this->assertContains(User::ROLE_USER, $roles);
        $this->assertNotContains(User::ROLE_ADMIN, $roles);
    }

    protected function logUser($client, $username)
    {
        $users = $client->getContainer()->get(UserRepository::class);
        $user = $users->findOneBy([
            'username' => $username
        ]);

        $session = $client->getContainer()->get('session');
        $token = new UsernamePasswordToken($user, null, 'main', $user->getRoles());
        $session->set('_security_main', serialize($token));
        $session->save();
        $cookie = new Cookie($session->getName(), $session->getId());
        $client->getCookieJar()->set($cookie);

        return $user;
    }
}